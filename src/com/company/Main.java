package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class Main {


    private static void recursion(int number, Stack<Integer> stack) {

        if (number != 0) {
            stack.push(number % 2);
            number /= 2;
            recursion(number, stack);
        } else return;

    }


    public static void main(String[] args) throws IOException {


        //Task 1
        System.out.println("Task 1");


        Stack<Integer> stack = new Stack<>();
        recursion(23213, stack);

        while (!stack.empty()) {
            System.out.print(stack.pop());
        }

        System.out.println("\nEnd Task 1\n");
        //End Task 1


        //Task 2
        System.out.println("Task 2\n");

        FIFO<Integer> fifTest = new FIFO<>();

        for (int i = 0; i < 10; i++) {
            fifTest.push(i);
        }

        while (!fifTest.isEmpty()) {
            System.out.println(fifTest.pop());
        }


        System.out.println("\nEnd Task 2\n");
        //End Task 2

    }


}
